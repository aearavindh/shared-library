def call(String msg="STAGE FAILED")
{
   /* jiraAddComment comment: msg, idOrKey:id , site: 'jira' */
 def issue = [fields: [ project: [key:'BMI'],
                       summary: 'BUG created',
                       description: '${msg}',
                       issuetype: [name: 'Bug']]]
def newIssue = jiraNewIssue issue: issue, site: 'jira'
echo newIssue.data.key

}
