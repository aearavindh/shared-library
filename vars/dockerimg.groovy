def call(String branch="development")
{
    withCredentials([usernamePassword(credentialsId: 'DockerHub_Credentials', passwordVariable: 'pass', usernameVariable: 'user')]) {
    withCredentials([string(credentialsId: 'ANSADMIN_PASSWORD', variable: 'ansadmin_password')]){
        sh 'sshpass -p ${ansadmin_password} scp -v Dockerfile ansadmin@172.31.22.13:/opt/playbooks'
        sh 'sshpass -p ${ansadmin_password} ssh -v -o StrictHostKeyChecking=no ansadmin@172.31.22.13 \"cd /opt/playbooks; wget -O BMI.war http://18.224.155.110:8081/nexus/content/repositories/devopstraining/comrades/bmi/BMI/BMI-${BUILD_NUMBER}.war; sudo service docker start; sudo docker build -t aearavindh/bmi:${BUILD_NUMBER} .; docker login -u ${user} -p ${pass}; sudo docker push aearavindh/bmi:${BUILD_NUMBER}\"'
    }
    }
}