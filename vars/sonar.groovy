def call(body){
  withSonarQubeEnv('sonarqube'){
     sh '${sonarscanner}/bin/sonar-scanner -Dproject.settings=./sonar-project.properties'
                     
  }
  timeout(time: 3, unit: 'MINUTES') {
     waitForQualityGate abortPipeline: true
  }
}
