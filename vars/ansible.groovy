def call(body)
{
    withCredentials([string(credentialsId: 'ANSADMIN_PASSWORD', variable: 'ansadmin_password')]){
        sh 'sshpass -p ${ansadmin_password} scp -v BMI-Playbook.yml ansadmin@172.31.22.13:/opt/playbooks'
        sh 'sshpass -p ${ansadmin_password} ssh -v -o StrictHostKeyChecking=no ansadmin@172.31.22.13 \"cd /opt/playbooks; ansible-playbook /opt/playbooks/BMI-Playbook.yml\"'
    }
}
